package com.example.task7.domain;

import org.springframework.lang.NonNull;

public class WebPage {

    private final String html;

    private final Link link;

    public WebPage(@NonNull final String html, @NonNull final Link link) {
        this.html = html;
        this.link = link;
    }

    public String receivePage() {
        return html;
    }

    public Link receiveLink() {
        return link;
    }
}
