package com.example.task7.domain;

import org.springframework.lang.NonNull;

public class DateFormat {
    private final String format;

    public DateFormat(@NonNull final String format) {
        this.format = format;
    }

    public String receiveDateFormat() {
        return format;
    }
}
