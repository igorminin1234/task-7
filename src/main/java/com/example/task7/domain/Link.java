package com.example.task7.domain;

import org.springframework.lang.NonNull;

public class Link {
    private final String address;

    public Link(final String address) {
        verify(address);
        this.address = address;
    }

    private void verify(@NonNull final String address) {
        if (!address.matches("https:\\/\\/[\\w.\\/]+")) {
            throw new IllegalArgumentException();
        }
    }

    public String receiveLinkAddress() {
        return address;
    }
}
