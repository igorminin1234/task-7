package com.example.task7.domain;

import org.springframework.lang.NonNull;

public class Regexp {

    private final String pattern;

    public Regexp(@NonNull final String pattern) {
        this.pattern = pattern;
    }

    public String receiveRegexp() {
        return pattern;
    }
}
