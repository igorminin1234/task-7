package com.example.task7.domain;

import org.springframework.lang.NonNull;


public class Article {
    private final String heading;
    private final Link link;

    public Article(@NonNull final String html, @NonNull final Link link) {
        this.link = link;
        this.heading = html;
    }

    public String receiveArticle() {
        return heading;
    }

    public String receiveLink() {
        return link.receiveLinkAddress();
    }
}
