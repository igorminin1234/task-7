package com.example.task7.service.parseService;

import com.example.task7.domain.Article;
import com.example.task7.domain.WebPage;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ParseArticleService {
    List<Article> parse(WebPage html);
}
