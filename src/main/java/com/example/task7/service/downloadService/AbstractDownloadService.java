package com.example.task7.service.downloadService;

import com.example.task7.domain.Link;
import com.example.task7.domain.WebPage;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public abstract class AbstractDownloadService implements WebPageDownloadService {

    protected HttpRequest sendRequest(final Link link) {
        return HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(link.receiveLinkAddress()))
                .setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11")
                .build();
    }

    protected WebPage getHTMLFromResponse(final Link link) throws IOException, InterruptedException {
        final HttpClient client = HttpClient.newHttpClient();
        return new WebPage(client.send(sendRequest(link), HttpResponse.BodyHandlers.ofString()).body(), link);
    }



}