package com.example.task7.service.downloadService;

import com.example.task7.domain.Link;
import com.example.task7.domain.WebPage;
import org.springframework.stereotype.Component;

@Component
public interface WebPageDownloadService {
    WebPage download();
    Link receiveLink();
}
