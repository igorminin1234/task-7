package com.example.task7.app.model.repositories;

import com.example.task7.app.model.entities.Article;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ArticleRepository extends JpaRepository<Article, Integer> {
    List<Article> findByLink(String link);
}
