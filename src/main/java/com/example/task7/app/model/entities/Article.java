package com.example.task7.app.model.entities;

import lombok.Getter;

import javax.persistence.*;

@Entity
@Getter
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    private String link;

    private String heading;

    protected Article() {
    }

    public Article(final String heading, final String link) {
        this.heading = heading;
        this.link = link;
    }

}
