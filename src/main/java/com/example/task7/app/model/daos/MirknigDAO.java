package com.example.task7.app.model.daos;

import com.example.task7.app.model.entities.Article;
import com.example.task7.app.model.repositories.ArticleRepository;
import com.example.task7.domain.Link;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;


@Component
public class MirknigDAO extends AbstractDAO {

    @Value("${mirknig.link}")
    public String mirknigAddress;

    private Link link;

    @PostConstruct
    public void init() {
        link = new Link(mirknigAddress);

    }

    public MirknigDAO(final ArticleRepository ar) {
        super(ar);
    }

    public List<Article> extractArticles() {
        return super.extractArticles(link);
    }

    public void insertArticles(List<Article> articles) {
        articles.forEach(super::insertArticle);
    }

    public Link receiveLink() {
        return link;
    }
}
