package com.example.task7.app.model.daos;

import com.example.task7.app.model.entities.Article;
import com.example.task7.app.model.repositories.ArticleRepository;
import com.example.task7.domain.Link;
import com.example.task7.domain.WebPage;

import java.util.List;

public abstract class AbstractDAO {

    final ArticleRepository ar;

    public AbstractDAO(final ArticleRepository ar) {
        this.ar = ar;
    }

    protected List<Article> extractArticles(Link link) {
        return ar.findByLink(link.receiveLinkAddress());
    }

    protected void insertArticle(Article article) {
        ar.save(article);
    }
}
