package com.example.task7.app.util;


import com.example.task7.domain.Link;

import java.util.ArrayList;
import java.util.List;

public class ArticlesConverter {

    public static com.example.task7.app.model.entities.Article convertDomainToEntity(final com.example.task7.domain.Article domainArticle) {
        return new com.example.task7.app.model.entities.Article(domainArticle.receiveArticle(), domainArticle.receiveLink());
    }

    public static com.example.task7.domain.Article convertEntityToDomain(final com.example.task7.app.model.entities.Article entityArticle) {
        return new com.example.task7.domain.Article(entityArticle.getHeading(), new Link(entityArticle.getLink()));
    }

    public static List<com.example.task7.app.model.entities.Article> convertDomainsToEntities(final List<com.example.task7.domain.Article> domainArticles) {
        final List<com.example.task7.app.model.entities.Article> articles = new ArrayList<>(domainArticles.size());
        for (com.example.task7.domain.Article article : domainArticles) {
            articles.add(convertDomainToEntity(article));
        }
        return articles;
    }

    public static List<com.example.task7.domain.Article> convertEntitiesToDomains(final List<com.example.task7.app.model.entities.Article> entityArticles) {
        final List<com.example.task7.domain.Article> articles = new ArrayList<>(entityArticles.size());
        for (com.example.task7.app.model.entities.Article article : entityArticles) {
            articles.add(convertEntityToDomain(article));
        }
        return articles;
    }

}
