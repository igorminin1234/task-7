package com.example.task7.app.controller;

import com.example.task7.app.model.daos.MirknigDAO;
import com.example.task7.app.model.daos.OpennetDAO;
import com.example.task7.app.util.ArticlesConverter;
import com.example.task7.domain.Article;
import com.example.task7.domain.Link;
import com.example.task7.domain.WebPage;
import com.example.task7.service.downloadService.MirknigDownloadServiceImpl;
import com.example.task7.service.downloadService.OpennetDownloadServiceImpl;
import com.example.task7.service.downloadService.WebPageDownloadService;
import com.example.task7.service.parseService.MirknigArticleParserImpl;
import com.example.task7.service.parseService.OpennetArticleParserImpl;
import com.example.task7.service.parseService.ParseArticleService;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@org.springframework.stereotype.Controller
@RequestMapping("/")
public class Controller {

    private final WebPageDownloadService opennetDownloader;

    private final WebPageDownloadService mirknigDownloader;

    private final ParseArticleService opennetParser;

    private final ParseArticleService mirknigParser;

    private final OpennetDAO opennetDAO;

    private final MirknigDAO mirknigDAO;

    public Controller(final OpennetDownloadServiceImpl opennetDownloader,
                      final MirknigDownloadServiceImpl mirknigDownloader,
                      final OpennetArticleParserImpl opennetParser,
                      final MirknigArticleParserImpl mirknigParser,
                      final OpennetDAO opennetDAO,
                      final MirknigDAO mirknigDAO) {
        this.opennetDownloader = opennetDownloader;
        this.mirknigDownloader = mirknigDownloader;
        this.opennetParser = opennetParser;
        this.mirknigParser = mirknigParser;
        this.opennetDAO = opennetDAO;
        this.mirknigDAO = mirknigDAO;
    }

    @GetMapping("/")
    public String openMainPage() {
        return "main";
    }

    @PostMapping("/")
    public String showTodaysArticles(@RequestParam(name = "website") final String websiteName, final Model model) {
        final WebPage page;
        final List<Article> articles;
        final Link link;
        if (websiteName.equals("opennet")) {
            if (opennetDAO.extractArticles().isEmpty()) {
                page = opennetDownloader.download();
                articles = opennetParser.parse(page);
                link = opennetDownloader.receiveLink();
                opennetDAO.insertArticles(ArticlesConverter.convertDomainsToEntities(articles));
            }
            else {
                articles = ArticlesConverter.convertEntitiesToDomains(opennetDAO.extractArticles());
                link = opennetDAO.receiveLink();
            }

        } else if (websiteName.equals("mirknig")) {
            if (mirknigDAO.extractArticles().isEmpty()) {
                page = mirknigDownloader.download();
                articles = mirknigParser.parse(page);
                link = mirknigDownloader.receiveLink();
                mirknigDAO.insertArticles(ArticlesConverter.convertDomainsToEntities(articles));
            }
            else {
                articles = ArticlesConverter.convertEntitiesToDomains(mirknigDAO.extractArticles());
                link = mirknigDAO.receiveLink();
            }
        } else {
            return "error";
        }

        model.addAttribute("headings", articles);
        model.addAttribute("link", link.receiveLinkAddress());

        return "articles";
    }


}
